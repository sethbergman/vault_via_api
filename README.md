### Example of using HashiCorp Vault's HTTP API to retrieve secrets
- [Vault Doc - JWT authentication via API](https://www.vaultproject.io/docs/auth/jwt#via-the-api)
- [Vault Doc - Retrieve KV (Key-Value) pair](https://www.vaultproject.io/api-docs/secret/kv/kv-v2#read-secret-version)
- [Video - Retrieve HashiCorp Vault Secret via API](https://www.youtube.com/watch?v=KxQVlrFy3Gc)

Benefit
- does not require Vault CLI to be installed

Prerequisite
- Create an environment variable called VAULT_ADDR whose value is the vault's address in the form http://127.0.0.1:8200
- enable JWT auth method in Vault
- configure policy and roles -- instructions can be found in the documentation for the CLI example

The CLI example, is documenteed in: [Authenticating with HashiCorp Vault](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/) in the GitLab docs